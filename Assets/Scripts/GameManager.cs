﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    AudioSource _audiosource;

    [SerializeField]
    PacmanMovement _pacman;

    [SerializeField]
    GameObject _gameOverUI;

    [SerializeField]
    AudioClip _audioStart;

    private void Awake()
    {
        _audiosource = GetComponent<AudioSource>();
    }

    bool _isGameReady;

    public bool isGameReady()
    {
        return _isGameReady;
    }



    // Use this for initialization
    void Start()


    {

        if (_pacman == null)
            throw new UnityException("Pacman is missing");

        _gameOverUI.SetActive(false);

        StartCoroutine(CheckStarMusic());

    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDefeat())
        {
            OnDefeat();
        }

        else if (CheckVictory())
        {
            OnVictory();

        }

    }
    void OnVictory()
    {
        Debug.Log("Hai Vinto");
        SceneManager.LoadScene("Stage 02");
    }

    void OnDefeat()

    {
        _gameOverUI.SetActive(true);
        _isGameReady = false;
    }

    //{
    //    throw new NotImplementedException();
    //}

    /// <summary>
    /// Controlla la condizione di vittoria.
    /// </summary>
    /// <returns></returns>True se il giocatore ha vinto, altrimenti false. <return/>
    bool CheckVictory()
    {
        return (_pacman.GetEatenPills() == Pill.GetTotalpillCount());

    }

    /// <summary>
    /// Controlla le condizione di sconfitta. (quando il giocatore arriva a zero vite)
    /// </summary>
    /// <returns></returns>
    bool CheckDefeat()
    {
        return (_pacman.GetLives() == 0);
    }

    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di permetere l'avvio del gioco.
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStarMusic()
    {
        _isGameReady = false;
        _audiosource.PlayOneShot(_audioStart);
        while (_audiosource.isPlaying)
            yield return null;
        _isGameReady = true;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("Stage 01");
    }


}
