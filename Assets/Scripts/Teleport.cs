﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{


    [SerializeField]
    Transform GoTo;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter( Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("ENTRO NEL TELETRANSPORTO!");

            collision.gameObject.transform.position = GoTo.position;
        }
    }

}
