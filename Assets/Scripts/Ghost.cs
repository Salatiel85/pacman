﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    static public int Points = 200;

    Renderer _ghostMeshRenderer;
    GameManager _gameManager;
    NavMeshAgent _navAgent;
    GameObject _player;
    PacmanMovement _pacman;

    [SerializeField]
    GameObject GhostMesh;

    [SerializeField]
    GameObject EyesMesh;

    [SerializeField]
    Transform SpawnPoint;

    bool _isDead;

    Collider _collider;

    [SerializeField]
    Material DefaultMaterial;

    [SerializeField]
    Material AfraidMaterial;

    void Awake()

    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _collider = gameObject.GetComponent<Collider>();
        _navAgent = GetComponent<NavMeshAgent>();

        _ghostMeshRenderer = GhostMesh.GetComponent<Renderer>();
        DefaultMaterial = _ghostMeshRenderer.material;


    }

    //void OnDisable()
    //{
    //    Destroy(GhostMesh);
    //}



    // Use this for initialization
    void Start()
    {

        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new UnityException("Player is missing");

        if (SpawnPoint == null)
            throw new UnityException("spaw point is missing");

        _pacman = _player.GetComponent<PacmanMovement>();
        EyesMesh.SetActive(false);

        Debug.Log("TOTALE PILLOLE=" + Pill.GetTotalpillCount());

    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.isGameReady() == false)
            return;

        GhostMesh.transform.position = transform.position;
        EyesMesh.transform.position = transform.position;

        if (!_isDead) // il fantasma e' vivo
        {
            _navAgent.SetDestination(_player.transform.position);

            SetMaterial();


        }

        else // il fantasma e' morto

        {
            if (isInsideSpawnZone())
            {
                //respwan
                Respawn();
            }
        }
        
    }

    private void Respawn()
    {
        GhostMesh.SetActive(true);
        EyesMesh.SetActive(false);
        _isDead = false;
        _collider.enabled = true;

        SetMaterial();
    }

    private void SetMaterial()
    {
        if (_pacman.HasPowerUp())
        {
            _ghostMeshRenderer.material = AfraidMaterial;
        }

        else
        {
            _ghostMeshRenderer.material = DefaultMaterial;
        }
    }




    bool isInsideSpawnZone()
    {
        return (gameObject.transform.position - SpawnPoint.transform.position).magnitude <= 0.5;
    }



    public void OnDeath()
    {
        _isDead = true;
        GhostMesh.SetActive(false);
        EyesMesh.SetActive(true);
        _navAgent.SetDestination(SpawnPoint.position);
        _collider.enabled = false;

    }


}
