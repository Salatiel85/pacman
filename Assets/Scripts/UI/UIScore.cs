﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScore : MonoBehaviour {

    [SerializeField]
    PacmanMovement pacman;

    [SerializeField]
    Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();
        if (_text == null)
        {
      
            throw new UnityException("Text component is missing");
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _text.text = pacman.TotalPoints.ToString();
		
	}
}
