﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanMovement : MonoBehaviour
{

    GameManager _gameManager;

    [SerializeField]
    float MovementSpeed;
    public int TotalPoints;

    int Lives = 3;

    public int GetLives()
    {
        return Lives;
    }


    AudioSource _audiosource;

    [SerializeField]
    AudioClip _audioStart;

    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _audioDeath;

    //bool _isGameReady;

    //public bool isGameReady()
    //{
    //    return _isGameReady;
    //}

    bool _hasPowerUp;

    public bool HasPowerUp()
    {
        return _hasPowerUp;
    }

    /// <summary>
    /// Tempo transcorso da quando si eì raccolto il PowerUp
    /// </summary>

    float _PowerUpElapsedTime = 0;

    /// <summary>
    /// indica ultimo PowerUp raccolto;
    /// </summary>

    float _PowerUpDuration = 10;

    /// <summary>
    /// Rapresenta il numero di fantasma mangiati durante lìeffeto del PowerUp
    /// p
    /// </summary>

    int _EatenGhost;

    int _eatenPills;

    public int GetEatenPills()
    {
        return _eatenPills;
    }


    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _audiosource = GetComponent<AudioSource>();
    }


    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {

        if (_gameManager.isGameReady() == false)
            return;

        float h = Input.GetAxis("Horizontal");

        float v = Input.GetAxis("Vertical");

        //Debug.Log("h:" + h + "-v:" + v);

        if (h != 0)

        {
            transform.Translate(Vector3.right * MovementSpeed * h);
        }
        else
        {

        }

        transform.Translate(Vector3.forward * MovementSpeed * v);

        if (h != 0 || v != 0)
        {
            if (!_audiosource.isPlaying)

                _audiosource.PlayOneShot(_audioMov);
        }

        if (_hasPowerUp)
        {
            _PowerUpElapsedTime += Time.deltaTime;
            Debug.Log("Elapsed Time" + _PowerUpElapsedTime);
            if (_PowerUpElapsedTime >= _PowerUpDuration)
            {
                _hasPowerUp = false;
                _EatenGhost = 0;
            }

        }


    }

    void OnTriggerEnter(Collider other)

    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");

        if (other.gameObject.tag == "Pill")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "Ghost")
        {

            if (!_hasPowerUp)
            {
                Onhit();
            }

            else
            {
                OnEatGhost(other);
            }

        }
    }
    /// <summary>
    /// Codice eseguito quando Pacman mangia un fantasma.
    /// </summary>

    private void OnEatGhost(Collider other)
    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");
        _EatenGhost++;

        TotalPoints += (int)Mathf.Pow(2, _EatenGhost - 1) * Ghost.Points;
        //Destroy(other.gameObject);
        other.gameObject.SendMessage("OnDeath", SendMessageOptions.RequireReceiver);
    }

    /// <summary>
    /// codice eseguito quando si mangia la palla.
    /// </summary>

    void OnEatPill(Collider other)
    {
        Debug.Log("GNAM");
        _eatenPills++;

        Pill pill = other.gameObject.GetComponent<Pill>();
        TotalPoints += pill.Points;

        if (pill is PowerUp)
        {
            Debug.Log("POWER UP!!!");
            _hasPowerUp = true;
            _PowerUpElapsedTime = 0;
        }
        Destroy(other.gameObject);

        if (_eatenPills == Pill.GetTotalpillCount())
            Debug.Log("HAI VINTO!");
    }

    /// <summary>
    /// Codice eseguito quando pacman viene colpito da un nemico.
    /// </summary>
    void Onhit()
    {
        Debug.Log("Game Over");
        if (!_audiosource.isPlaying)
        {
            _audiosource.PlayOneShot(_audioDeath);
        }

        Lives -= 1;
    }

}









